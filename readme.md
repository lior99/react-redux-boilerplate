# React + Redux boilerplate

## Prerequisits

* node ( 6.9 or later)
* yarn ( 0.22 or later)
* npm 


### Installation

yarn is faster than npm, so it is recommended to use it.
if you do not have yarn installed than install or use npm


using yarn : 

1. run yarn install
2. run yarn start

using npm :

1. npm install
2. npm start

###  open browser with port 5000
port number can be changed to other one in package.json